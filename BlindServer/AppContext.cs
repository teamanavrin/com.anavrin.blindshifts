﻿using BlindServer.Properties;
using System;
using System.Diagnostics;
using System.Media;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlindServer
{
    // State object for reading client data asynchronously  
    public class StateObject
    {
        // Size of receive buffer.  
        public const int BufferSize = 1024;

        // Receive buffer.  
        public byte[] buffer = new byte[1024];

        // Received data string.
        public StringBuilder sb = new StringBuilder();

        // Client socket.
        public Socket workSocket = null;
    }
    
    public class AppContext : ApplicationContext
    {
        private readonly NotifyIcon _trayIcon;
        private readonly ContextMenuStrip _menu = new ContextMenuStrip();
        public static ManualResetEvent allDone = new ManualResetEvent(false);

        public AppContext()
        {
          
            _trayIcon = new NotifyIcon()
            {
                Icon = Resources.AppIcon,
                Text = "Server System Agent",
                Visible = true
            };
            _trayIcon.MouseClick += _trayIcon_MouseClick;

            var menuExit = new ToolStripMenuItem("Salir") { Image = Resources.submenu5 };
            menuExit.Click += MenuExitOnClick;
            _menu.Items.Add(menuExit);

            StartListening();

            _trayIcon.ContextMenuStrip = _menu;
        }

        private void _trayIcon_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    break;
                case MouseButtons.Right:
                    _trayIcon.Visible = true;
                    _menu.Show(Cursor.Position.X, Cursor.Position.Y);
                    break;
                default:
                    break;
            }
        }

        private void MenuExitOnClick(object sender, EventArgs e)
        {
            _trayIcon.Visible = false;
            try
            {
                Process.GetCurrentProcess().Kill();
            }
            catch (Exception exception)
            {
             
            }
            Application.Exit();
        }

        public void StartListening()
        {
            // Establish the local endpoint for the socket.  
            // The DNS name of the computer  
            // running the listener is "host.contoso.com".
            var port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Port"]);
            var ipAddress = IPAddress.Any;
            var localEndPoint = new IPEndPoint(ipAddress, port);

            // Create a TCP/IP socket.  
            var listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.  
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100);

                while (true)
                {
                    // Set the event to nonsignaled state.  
                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.  
                    Debug.WriteLine("Waiting for a connection...");
                    listener.BeginAccept(AcceptCallback, listener);

                    // Wait until a connection is made before continuing.  
                    allDone.WaitOne();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }

            Debug.WriteLine("\nPress ENTER to continue...");
            Console.Read();
        }

        public void AcceptCallback(IAsyncResult ar)
        {
            // Signal the main thread to continue.  
            allDone.Set();

            // Get the socket that handles the client request.  
            var listener = (Socket)ar.AsyncState;
            var handler = listener.EndAccept(ar);

            // Create the state object.  
            StateObject state = new StateObject();
            state.workSocket = handler;
            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReadCallback), state);
        }

        public void ReadCallback(IAsyncResult ar)
        {
            String content = String.Empty;

            // Retrieve the state object and the handler socket  
            // from the asynchronous state object.  
            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.workSocket;

            // Read data from the client socket.
            int bytesRead = handler.EndReceive(ar);

            if (bytesRead > 0)
            {
                // There  might be more data, so store the data received so far.  
                state.sb.Append(Encoding.ASCII.GetString(
                    state.buffer, 0, bytesRead));

                // Check for end-of-file tag. If it is not there, read
                // more data.  
                content = state.sb.ToString();
                if (content.IndexOf("<EOF>", StringComparison.Ordinal) > -1)
                {
                    content = content.Replace("<EOF>","");
                    Task.Run(() => MessageBox.Show($"Se solicita asistencia en el Kiosco {content}"));
                    Task.Run(() =>
                    {
                        var simpleSound = new SoundPlayer(@"alert.wav");
                        simpleSound.Play();
                    });
                }
                else
                {
                    // Not all data received. Get more.  
                    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);
                }
            }
        }
    }
}
