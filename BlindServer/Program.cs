﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace BlindServer
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (!AmIRunning())
            {
                Application.Run(new AppContext());
            }
        }
        public static bool AmIRunning()
        {
            var processName = Process.GetCurrentProcess().ProcessName;
            var allProcessWithThisName = Process.GetProcessesByName(processName);
            return allProcessWithThisName.Length > 1;
        }
    }
}
