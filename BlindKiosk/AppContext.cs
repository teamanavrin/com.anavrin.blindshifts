﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using BlindKiosk.Properties;
using IniParser;
using IniParser.Model;

namespace BlindKiosk
{
    public class AppContext : ApplicationContext
    {
        private ContextMenuStrip menu = new ContextMenuStrip();
        private FileIniDataParser Parser { get; }
        private IniData IniData { get; }
        private readonly NotifyIcon _trayIcon;
        private readonly ContextMenuStrip _menu = new ContextMenuStrip();
        private SerialPort _serialPort;
        private readonly int _maxEvents;
        private string _lastEvent;
        private int _eventCounter;
        private bool _isBusy = false;
        private Receptionist receptionist;

        public AppContext()
        {
            Parser = new FileIniDataParser();
            IniData = Parser.ReadFile("Configuration.ini");

            Logger.Info("Starting App");

            var portNames = SerialPort.GetPortNames();
            if (!portNames.Any())
            {
                Logger.Info("No devices detected");
                MessageBox.Show(
                    "No se detectaron puertos COM conectados.Por favor revisa la conexión y vuelve a iniciar el programa", "Agente", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
                return;
            }

            var arrivalButton = IniData["Ports"]["ArrivalButtonPort"];
            if (!portNames.Contains(arrivalButton))
            {
                Logger.Info("No arrival button port detected");
                MessageBox.Show(
                    $"No se detectó ningún dispositivo en el puerto: { arrivalButton}. Revise el archivo Configuration.ini y reinice el agente", "Agente", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
                return;
            }

            _lastEvent = string.Empty;
            _eventCounter = 0;
            _maxEvents = 1;

            _trayIcon = new NotifyIcon()
            {
                Icon = Resources.AppIcon,
                Text = "Client System Agent",
                Visible = true
            };
            _trayIcon.MouseClick += _trayIcon_MouseClick;

            var menuExit = new ToolStripMenuItem("Salir") { Image = Resources.submenu5 };
            menuExit.Click += MenuExitOnClick;
            menu.Items.Add(menuExit);

            var menuSimulate = new ToolStripMenuItem("Simular llamado") {  };
            menuSimulate.Click += MenuSimulate_Click;
            menu.Items.Add(menuSimulate);

            receptionist = new Receptionist(Convert.ToInt32(IniData["Ports"]["CommunicationPort"]));

            _serialPort = new SerialPort(IniData["Ports"]["ArrivalButtonPort"], 9600, Parity.None, 8, StopBits.One);
            _serialPort.DataReceived += SerialPort_DataReceived;
            _trayIcon.ContextMenuStrip = menu;
            StartTrigger();
        }

        private async void MenuSimulate_Click(object sender, EventArgs e)
        {
            try
            {
                await receptionist.ReceiveVisitor();
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
        }

        private void MenuExitOnClick(object sender, EventArgs e)
        {
            _trayIcon.Visible = false;
            try
            {
                Process.GetCurrentProcess().Kill();
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
            Application.Exit();
        }
        private async void StartTrigger()
        {
            while (!_serialPort.IsOpen)
            {
                try
                {
                    _serialPort.Open();
                    Thread.Sleep(100);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                }
            }
            await Task.Run(() =>
            {
                while (!_isBusy)
                {
                }
            });
            _isBusy = false;
            _eventCounter = 0;
            Debug.WriteLine("Receptionist started");
            try
            {
                await receptionist.ReceiveVisitor();
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
            Debug.WriteLine("Receptionist finished");
            Thread.Sleep(20000);
            StartTrigger();
        }
        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            
            _lastEvent = _serialPort.ReadLine();
            Debug.WriteLine(_lastEvent);
           
            if (_lastEvent.Trim().Equals("Start", StringComparison.OrdinalIgnoreCase))
            {
                _eventCounter++;

            }
            else
            {
                _eventCounter = 0;
            }
            if (_eventCounter == _maxEvents)
            {
                _serialPort.Close();
                _serialPort.Dispose();
                _isBusy = true;
            }
        }
        private void _trayIcon_MouseClick(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Left:
                    break;
                case MouseButtons.Right:
                    _trayIcon.Visible = true;
                    _menu.Show(Cursor.Position.X, Cursor.Position.Y);
                    break;
                default:
                    break;
            }
        }
    }
}