﻿using System;
using System.Diagnostics;

namespace BlindKiosk
{
    internal class Logger
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger("LogFileAppender");
        public static void Info(string message)
        {
            Debug.WriteLine(message);
            Log.Info(message);
        }
        public static void Error(string error)
        {
            Debug.WriteLine(error);
            Log.Error(error);
        }
        public static void Error(Exception exception)
        {
            Debug.WriteLine(exception.Message);
            Log.Error(exception.Message, exception);
            if (exception.InnerException != null)
            {
                Log.Error(exception.InnerException.Message, exception.InnerException);
            }
        }
    }
}
