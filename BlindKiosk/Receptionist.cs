﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using IniParser;
using IniParser.Model;
using System.Speech.Synthesis;

namespace BlindKiosk
{
    internal class Receptionist
    {
        private SpeechSynthesizer Synthesizer { get; }
        private FileIniDataParser Parser { get; }
        private IniData IniData { get; }
        private int Port { get; }

        public Receptionist(int port)
        {
            Parser = new FileIniDataParser();
            IniData = Parser.ReadFile("Configuration.ini");
            Synthesizer = new SpeechSynthesizer();
            Port = port;

            try
            {
                Synthesizer.SelectVoice(IniData["General"]["MicrosoftVoice"]);
            }
            catch (Exception e)
            {
                Logger.Error(e);
                Synthesizer.SetOutputToDefaultAudioDevice();
            }
        }

        #region Socket Communication
        private static ManualResetEvent sendDone = new ManualResetEvent(false);

        // The response from the remote device.  
        private static readonly string response = string.Empty;

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket client = (Socket) ar.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = client.EndSend(ar);
                Debug.WriteLine("Sent {0} bytes to server.", bytesSent);

                // Signal that all bytes have been sent.  
                sendDone.Set();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }

        #endregion

        public async Task ReceiveVisitor()
        {
            #region Send signal to admin

            var serverIps = IniData["General"]["ServerIPs"];

            var ips = serverIps.Split(';');

            foreach (var ip in ips)
            {
                // Connect to a remote device.  
                try
                {
                    var cleanIp = ip.Trim();

                    var ipAddress = IPAddress.Parse(cleanIp);

                    var endPoint = new IPEndPoint(ipAddress, Port);

                    // Create a TCP/IP socket.  
                    using (var socket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp))
                    {
                        // Connect to the remote endpoint.  
                        await socket.ConnectAsync(endPoint);

                        // Convert the string data to byte data using ASCII encoding.  
                        var byteData = Encoding.ASCII.GetBytes($"{IniData["General"]["KioskID"]}<EOF>");

                        // Begin sending the data to the remote device.  
                        socket.BeginSend(byteData, 0, byteData.Length, 0, SendCallback, socket);
                        sendDone.WaitOne();

                        // Write the response to the console.  
                        Debug.WriteLine($"Response received : {response}");

                        // Release the socket.  
                        socket.Shutdown(SocketShutdown.Both);
                        socket.Close();
                    }
                }
                catch (Exception e)
                {
                    Logger.Error(e);
                }
            }
          

            #endregion

            var prompt = Synthesizer.SpeakAsync(IniData["General"]["TextToSay"]);
        }
    }
}
